#!/bin/sh

if [ $# -eq 0 ]; then
DIR=`pwd`
if [ "$DIR" = *develconfig ]; then
    cd ..
    DIR=`pwd`
fi
else
DIR=$1
fi

export HOME=$DIR
cd $HOME
ID=`echo ${PWD##*/}`

cp -fp develconfig/myrc develconfig/myrc_local
cp -fp develconfig/gitconfig develconfig/gitconfig_local
sed -i "s|HOME_PASS_HERE|$DIR|g" develconfig/myrc_local
sed -i "s/ID_HERE/$ID/g" develconfig/myrc_local
sed -i "s/ID_HERE/$ID/g" develconfig/gitconfig_local

echo "디렉토리 생성..."
mkdir -p $HOME/.zsh

echo "심볼릭 릭크 생성..."
ln -sfv ~/develconfig/tmux.conf ~/.tmux.conf
ln -sfv ~/develconfig/screenrc ~/.screenrc
ln -sfv ~/develconfig/zshrc ~/.zshrc
ln -sfv ~/develconfig/vimrc ~/.vimrc
ln -sfv ~/develconfig/myrc_local ~/rundevel
ln -sfv ~/develconfig/gitconfig_local ~/.gitconfig
ln -sfv ~/develconfig/gitignore_global ~/.gitignore_global

SUDO=""
if [ "$ID" != "root" ]; then
    SUDO=`which sudo`
fi

if [ ! -f /etc/zshrc ]; then
    $SUDO cp -fp ~/develconfig/etc_zshrc /etc/zshrc
fi

if [ -e ~/.vim ]; then
	echo "경고: 설치를 진행하려면 ~/.vim/ 디렉토리를 삭제해야 합니다."
	exit
fi

noUbuntu=`uname -a | grep -i -e ubuntu -e microsoft`
VI=`which vi`
if [ "$noUbuntu" = "" ]; then
echo "vim을 설치 합니다."
yum install -y ncurses-devel
git clone https://github.com/vim/vim.git
cd vim
./configure --without-x --enable-cscope --enable-multibyte --enable-hangulinput --enable-pythoninterp=yes --enable-python3interp=yes
$SUDO make
$SUDO make install
VI="/usr/local/bin/vim"
else
ZSHELL=`which zsh 2>&1`
if [ "$ZSHELL" = "" ]; then
    $SUDO apt-get --assume-yes install zsh
fi
TMUXBIN=`which tmux 2>&1`
if [ "$TMUXBIN" = "" ]; then
    $SUDO apt-get --assume-yes install tmux
fi
$SUDO apt-get --assume-yes install fonts-powerline
fi
cd $HOME

export GIT_SSL_NO_VERIFY=true
echo "vundle 다운로드중..."
git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

echo "vundle 설치중..."
$VI -c :BundleInstall -c :qa

if [ 0 ]; then
echo "YouCompleteMe 추가 설치중..."
cd $HOME/.vim/bundle/YouCompleteMe
git submodule update --init --recursive
$HOME/.vim/bundle/YouCompleteMe/install.sh
fi

echo "shell Color theme 설치"
#git clone https://github.com/seebi/dircolors-solarized.git $HOME/dircolors-solarized
#ln -sf $HOME/dircolors-solarized/dircolors.ansi-dark $HOME/.dir_colors
git clone https://github.com/arcticicestudio/nord-dircolors.git $HOME/nord-dircolors
ln -sf $HOME/nord-dircolors/src/dir_colors $HOME/.dir_colors

source $HOME/develconfig/goinstall.sh
